import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        DateFormat dateFormat = null ;
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        System.out.println("Hello, World!");
        Customer customer1 = new Customer();
        Customer customer2 = new Customer("gà",true,"gia cầm");

        Visit visit1 = new Visit(customer1,new Date(),20000,30000);
        Visit visit2 = new Visit(customer2,dateFormat.parse("15/07/2022") ,50000,30000);
        System.out.println(visit1.toString());
        System.out.println(visit2.toString());
        
    }
}
